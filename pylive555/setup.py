from distutils.core import setup, Extension

INCLUDE_DIR = '/usr/include'
INSTALL_DIR = './live'
dirs=['%s/%s' % (INCLUDE_DIR, x) for x in ['liveMedia', 'BasicUsageEnvironment', 'UsageEnvironment', 'groupsock']]
dirs.extend(['%s/%s/include' % (INSTALL_DIR, x) for x in ['liveMedia', 'BasicUsageEnvironment', 'UsageEnvironment', 'groupsock']])
module = Extension('live555',
                   include_dirs=dirs,
                   libraries=['liveMedia', 'groupsock', 'BasicUsageEnvironment', 'UsageEnvironment'],
                   #extra_compile_args = ['-fPIC'],
                   library_dirs=['%s/%s' % (INSTALL_DIR, x) for x in ['liveMedia', 'UsageEnvironment','BasicUsageEnvironment', 'groupsock']],
                   sources = ['module.cpp'])
  
setup(name = 'live555',
      version = '1.0',
      description = 'Basic wrapper around live555 to load RTSP video streams',
      ext_modules = [module])
